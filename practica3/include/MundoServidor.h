// Mundo.h: interface for the CMundo class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_MUNDO_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_)
#define AFX_MUNDO_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_

#include <vector>
#include "Plano.h"

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <stdio.h>
#include <unistd.h>
#include <iostream>
#include "DatosMemCompartida.h"
#include <pthread.h>

class CMundoServidor
{
public:
	void Init();
	CMundoServidor();
	virtual ~CMundoServidor();	
	
	void InitGL();	
	void OnKeyboardDown(unsigned char key, int x, int y);
	void OnTimer(int value);
	void OnDraw();	

	Esfera esfera;
	std::vector<Plano> paredes;
	Plano fondo_izq;
	Plano fondo_dcho;
	Raqueta jugador1;
	Raqueta jugador2;
	
	int cont;
	int puntos1;
	int puntos2;

//practica2

	int tuberia;

//PRACTICA3 TUBERIA PARA CLIENTE
	int tuberia2;
//PRACTICA3 TUBERIA TECLAS PARA CLIENTE PARA QUE SEPA POSICION
	int tuberia3;
	//PRACTICA3 CREAMOS THREAD PROCESO LIGERO
	pthread_t thd1;

//PRACTICA3 DECLARACION RECIBEDATOS
	void RecibeComandosJugador();

//PRACTICA3 HILO COMANDOS INLINE
	/*void* hilo_comandos(void* d)
	{
	      CMundoServidor* p=(CMundoServidor*) d;//mirar aqui
	      p->RecibeComandosJugador();
	}
*/
	DatosMemCompartida datos;
	DatosMemCompartida *dat;

};

#endif // !defined(AFX_MUNDO_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_)
