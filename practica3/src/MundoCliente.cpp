// Mundo.cpp: implementation of the CMundo class.
//
//////////////////////////////////////////////////////////////////////
#include <fstream>
#include "MundoCliente.h"
#include "glut.h"
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////
char *proy;
CMundoCliente::CMundoCliente()
{
	Init();
}

CMundoCliente::~CMundoCliente()
{
	munmap(proy,sizeof(datos));
	/*close(tuberia2);
	unlink("/tmp/TuberiaServidor");
	close(tuberia3);
	unlink("/tmp/TuberiaTeclas");*/
}

void CMundoCliente::InitGL()
{
	//Habilitamos las luces, la renderizacion y el color de los materiales
	glEnable(GL_LIGHT0);
	glEnable(GL_LIGHTING);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_COLOR_MATERIAL);
	
	glMatrixMode(GL_PROJECTION);
	gluPerspective( 40.0, 800/600.0f, 0.1, 150);
}

void print(char *mensaje, int x, int y, float r, float g, float b)
{
	glDisable (GL_LIGHTING);

	glMatrixMode(GL_TEXTURE);
	glPushMatrix();
	glLoadIdentity();

	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	gluOrtho2D(0, glutGet(GLUT_WINDOW_WIDTH), 0, glutGet(GLUT_WINDOW_HEIGHT) );

	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();

	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_BLEND);
	glColor3f(r,g,b);
	glRasterPos3f(x, glutGet(GLUT_WINDOW_HEIGHT)-18-y, 0);
	int len = strlen (mensaje );
	for (int i = 0; i < len; i++) 
		glutBitmapCharacter (GLUT_BITMAP_HELVETICA_18, mensaje[i] );
		
	glMatrixMode(GL_TEXTURE);
	glPopMatrix();

	glMatrixMode(GL_PROJECTION);
	glPopMatrix();

	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();

	glEnable( GL_DEPTH_TEST );
}
void CMundoCliente::OnDraw()
{
	//Borrado de la pantalla	
   	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	//Para definir el punto de vista
	glMatrixMode(GL_MODELVIEW);	
	glLoadIdentity();
	
	gluLookAt(0.0, 0, 17,  // posicion del ojo
		0.0, 0.0, 0.0,      // hacia que punto mira  (0,0,0) 
		0.0, 1.0, 0.0);      // definimos hacia arriba (eje Y)    

	/////////////////
	///////////
	//		AQUI EMPIEZA MI DIBUJO
	char cad[100];
	sprintf(cad,"Jugador1: %d",puntos1);
	print(cad,10,0,1,1,1);
	sprintf(cad,"Jugador2: %d",puntos2);
	print(cad,650,0,1,1,1);
	int i;
	for(i=0;i<paredes.size();i++)
		paredes[i].Dibuja();

	fondo_izq.Dibuja();
	fondo_dcho.Dibuja();
	jugador1.Dibuja();
	jugador2.Dibuja();
	esfera.Dibuja();

	/////////////////
	///////////
	//		AQUI TERMINA MI DIBUJO
	////////////////////////////

	//Al final, cambiar el buffer
	glutSwapBuffers();
}

void CMundoCliente::OnTimer(int value)
{	
											/*
	//PRACTICA2 BOT
	datos.esfera.centro.x=esfera.centro.x;
	datos.esfera.centro.y=esfera.centro.y;
	datos.raqueta1.y1=jugador1.y1;
	datos.raqueta1.y2=jugador1.y2;
	//
	cont+=1;
	jugador1.Mueve(0.025f);
	jugador2.Mueve(0.025f);
	esfera.Mueve(0.025f);
	int i;
	for(i=0;i<paredes.size();i++)
	{
		paredes[i].Rebota(esfera);
		paredes[i].Rebota(jugador1);
		paredes[i].Rebota(jugador2);
	}

	jugador1.Rebota(esfera);
	jugador2.Rebota(esfera);
	if(fondo_izq.Rebota(esfera))
	{
		esfera.centro.x=0;
		esfera.centro.y=rand()/(float)RAND_MAX;
		esfera.velocidad.x=2+2*rand()/(float)RAND_MAX;
		esfera.velocidad.y=2+2*rand()/(float)RAND_MAX;
		esfera.radio=0.5;
		puntos2++;                                  				*/
//PRACTICA2-TUBERIA LOGGER
		/*char cadena[200];
		sprintf(cadena,"Jugador 2 marco 1 punto, lleva %d",puntos2);
		write(tuberia,cadena,strlen(cadena)+1);
//
	}*/
									/*											
	if(fondo_dcho.Rebota(esfera))
	{
		esfera.centro.x=0;
		esfera.centro.y=rand()/(float)RAND_MAX;
		esfera.velocidad.x=-2-2*rand()/(float)RAND_MAX;
		esfera.velocidad.y=-2-2*rand()/(float)RAND_MAX;
		esfera.radio=0.5;
		puntos1++;									*/
//PRACTICA2-TUBERIA LOGGER
		/*char cadena[200];
		sprintf(cadena,"Jugador 1 marco 1 punto, lleva %d",puntos1);
		write(tuberia,cadena,strlen(cadena)+1); // write (donde , el que, tam )*/
//
											/*
	}
	if(cont>=50)
	{
	esfera.radio-=0.05;
	cont=0;
	}
	if(esfera.radio<0.1)
	esfera.radio=0.5;
													*/
//PACTICA2-BOT
	
	
	switch (dat->accion)
	{
		case 1:	OnKeyboardDown('w',0,0); break;
			//esto es lo que faltaba
		case -1: OnKeyboardDown('s',0,0); break;
		case 0:	break;
	}
	dat->esfera=esfera;
	dat->raqueta1=jugador1;
//Practica2-LOGGER PERO LO DEJO 
	if(puntos1==6 || puntos2==6){
		
		exit(0);
	}

//PRACTICA3 LEER TUBERIA
	char cad[200];
	read(tuberia2,cad,sizeof(cad));
	sscanf(cad,"%f %f %f %f %f %f %f %f %f %f %d %d", &esfera.centro.x,&esfera.centro.y, 				&jugador1.x1,&jugador1.y1,&jugador1.x2,&jugador1.y2, &jugador2.x1,&jugador2.y1,&jugador2.x2,&jugador2.y2, &puntos1, &puntos2);
	std::cout << "CS: " << cad << "\n" ;
}


void CMundoCliente::OnKeyboardDown(unsigned char key, int x, int y)
{
	
	switch(key)
	{
//	case 'a':jugador1.velocidad.x=-1;break;
//	case 'd':jugador1.velocidad.x=1;break;
	case 's':jugador1.velocidad.y=-4;break;
	case 'w':jugador1.velocidad.y=4;break;
	case 'l':jugador2.velocidad.y=-4;break;
	case 'o':jugador2.velocidad.y=4;break;

	}
	char cad[5];
	sprintf(cad,"%c",key);
	write(tuberia3,cad,sizeof(cad)+1); //como sea estoo
	std::cout<<cad<<std::endl;
}

void CMundoCliente::Init()
{
	Plano p;
//pared inferior
	p.x1=-7;p.y1=-5;
	p.x2=7;p.y2=-5;
	paredes.push_back(p);

//superior
	p.x1=-7;p.y1=5;
	p.x2=7;p.y2=5;
	paredes.push_back(p);

	fondo_izq.r=0;
	fondo_izq.x1=-7;fondo_izq.y1=-5;
	fondo_izq.x2=-7;fondo_izq.y2=5;

	fondo_dcho.r=0;
	fondo_dcho.x1=7;fondo_dcho.y1=-5;
	fondo_dcho.x2=7;fondo_dcho.y2=5;

	//a la izq
	jugador1.g=0;
	jugador1.x1=-6;jugador1.y1=-1;
	jugador1.x2=-6;jugador1.y2=1;

	//a la dcha
	jugador2.g=0;
	jugador2.x1=6;jugador2.y1=-1;
	jugador2.x2=6;jugador2.y2=1;

	//PRACTICA2
			
	

	//FICHERO-BOT

	int fd;

	fd=open("/tmp/datosBot.txt", O_RDWR | O_CREAT | O_TRUNC, 0777);

	write(fd,&datos,sizeof(datos)); 			

		//Se proyecta el fichero 

 	proy=(char*)mmap(NULL,sizeof(*(dat)), PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0); 
			
        	//Se cierra el fichero 

  	close(fd);

 	dat=(DatosMemCompartida*)proy;

	dat->accion=0;

	//PRACTICA3
	mkfifo("/tmp/TuberiaTecla",0777);//PRACTICA3 CREAMOS TUBERIA PARA ENVIAR TECLAS
	tuberia3 = open("/tmp/TuberiaTecla", O_RDWR);
printf("aquin\n");
	mkfifo("/tmp/TuberiaServidor",0666);//PRACTICA3 crea tuberia para leer las coordenadas que servidor envia
	tuberia2 = open("/tmp/TuberiaServidor", O_RDWR);

	
	
	
	
}	
