//Practica2
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdio.h>
#include <unistd.h>

int main(int argc,char* argv[])
{
	mkfifo("/tmp/TuberiaLogger",0777);
	int pipe = open("/tmp/TuberiaLogger", O_RDONLY);

	while(1)
	{
		char cad[200];
		read(pipe, cad, sizeof(cad)); // read (el que leo, donde lo pongo , tam )
		printf("%s\n", cad);
	}
	close(pipe);
	unlink("/tmp/TuberiaLogger");
	return 0;
}
